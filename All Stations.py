import requests

r = requests.get("https://api.tfl.gov.uk/Line/Mode/tube")
lineIDs = r.json()

stationDict = {}

for lineID in lineIDs:
	id = lineID["id"]
	r = requests.get("https://api.tfl.gov.uk/line/"+id+"/stoppoints")
	stations = r.json()
	stationCount = len(stations)

	for station in stations:
		stationName = station["commonName"]
		lineGroups   = station["lineModeGroups"]
		for lineGroup in lineGroups:
			modeName = lineGroup["modeName"]
			if (modeName=="tube"):
				lineCount = len(lineGroup["lineIdentifier"])

		stationDict[stationName] = lineCount
		sorted_d = sorted(stationDict.items(), key=lambda x: x[1])

		print(sorted_d)

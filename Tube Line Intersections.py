import requests, json

# Get all the current Tube lines
r = requests.get("https://api.tfl.gov.uk/Line/Mode/tube")
lineIDs = r.json()

# Set up a dict of dicts for each line
stationDict = {}
for lineID in lineIDs:
	id = lineID["id"]
	stationDict[id] = {}

# Get the stations for each line
for lineID in lineIDs:
	id = lineID["id"]
	r = requests.get("https://api.tfl.gov.uk/line/"+id+"/stoppoints")
	stations = r.json()
	# Populate the dict so we can see how many times the lines intersect
	for station in stations:
		stationName = station["commonName"]
		lineGroups   = station["lineModeGroups"]
		for lineGroup in lineGroups:
			modeName = lineGroup["modeName"]
			if (modeName=="tube"):
				lineNames = lineGroup["lineIdentifier"]
				for lineName in lineNames:
					# print("On the " + id + " line at " + stationName + " which is also on the " + lineName + " line")
					if lineName in stationDict[id]:
						stationDict[id][lineName] += 1
					else:
						stationDict[id][lineName] = 1

# print(stationDict)
print(json.dumps(stationDict))

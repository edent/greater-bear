import requests, json

def sort_by_values_len(dict):
	dict_len= {key: len(value) for key, value in dict.items()}
	import operator
	sorted_key_list = sorted(dict_len.items(), key=operator.itemgetter(1), reverse=True)
	sorted_dict = [{item[0]: dict[item [0]]} for item in sorted_key_list]
	return sorted_dict

# Get all the current Tube lines
r = requests.get("https://api.tfl.gov.uk/Line/Mode/tube")
lineIDs = r.json()

lineCountDict = {}

# Set up a dict of dicts for each line
stationDict = {}
for lineID in lineIDs:
	id = lineID["id"]
	stationDict[id] = {}

s={}

# Get the stations for each line
for lineID in lineIDs:
	id = lineID["id"]
	r = requests.get("https://api.tfl.gov.uk/line/"+id+"/stoppoints")
	stations = r.json()
	# Populate the dict so we can see how many times the lines intersect
	for station in stations:
		stationName = station["commonName"]
		s[stationName] = {}
		lineGroups   = station["lineModeGroups"]
		for lineGroup in lineGroups:
			modeName = lineGroup["modeName"]
			if (modeName=="tube"):
				lineCount = len(lineGroup["lineIdentifier"])
				lineNames = lineGroup["lineIdentifier"]
				for lineName in lineNames:
					# print("On the " + id + " line at " + stationName + " which is also on the " + lineName + " line")
					if lineName in stationDict[id]:
						stationDict[id][lineName] = ""
						s[stationName][lineName] = ""
					else:
						stationDict[id][lineName] = ""
						s[stationName][lineName] = ""

# print (sort_by_values_len(s))
print(json.dumps(sort_by_values_len(s)))

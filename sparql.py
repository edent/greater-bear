from SPARQLWrapper import SPARQLWrapper, JSON
import requests, json
import sys, time

def getData(sparqlQuery) :
	sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
	sparql.setQuery(sparqlQuery)
	sparql.setReturnFormat(JSON)
	sparql.addCustomHttpHeader("User-Agent","edent-test")
	results = sparql.query().convert()
	return results

def sort_by_values_len(dict):
	dict_len= {key: len(value) for key, value in dict.items()}
	import operator
	sorted_key_list = sorted(dict_len.items(), key=operator.itemgetter(1), reverse=True)
	sorted_dict = [{item[0]: dict[item [0]]} for item in sorted_key_list]
	return sorted_dict

# Get all the current Tube lines
lineData = requests.get("https://api.tfl.gov.uk/Line/Mode/tube")
lineIDs  = lineData.json()

lineCountDict = {}

# Set up a dict of dicts for each line
stationDict = {}
for lineID in lineIDs:
	id = lineID["id"]
	stationDict[id] = {}

stationsAndLines = {}

# Get the stations for each line
for lineID in lineIDs:
	id = lineID["id"]
	stopData = requests.get("https://api.tfl.gov.uk/line/"+id+"/stoppoints")
	stations = stopData.json()
	# Populate the dict so we can see how many times the lines intersect
	for station in stations:
		stationName = station["commonName"]
		stationsAndLines[stationName] = {}
		lineGroups = station["lineModeGroups"]
		for lineGroup in lineGroups:
			modeName = lineGroup["modeName"]
			if (modeName=="tube"):
				lineCount = len(lineGroup["lineIdentifier"])
				lineNames = lineGroup["lineIdentifier"]
				for lineName in lineNames:
					# print("On the " + id + " line at " + stationName + " which is also on the " + lineName + " line")
					if lineName in stationDict[id]:
						stationDict[id][lineName] = ""
						stationsAndLines[stationName][lineName] = ""
					else:
						stationDict[id][lineName] = ""
						stationsAndLines[stationName][lineName] = ""

stationsAndLines = sort_by_values_len(stationsAndLines)

#	To hold the stations
StationPersonMap = {} # {"Aldgate East":{"Q123":"Ginger Rogers"}, "Bank":{"Q567":"Walt Disney"}}

# Set up the variables
BakerlooQ           = "?person wdt:P108 wd:Q35794   . " #Employed by Cambridge University
CircleQ             = "?person wdt:P19 ?pob . ?pob wdt:P131* wd:Q145 ." #Born in UK or historic UKs
HammersmithAndCityQ = "?person wdt:P463 wd:Q123885  . " #Member of the Royal Society
WaterlooAndCityQ    = "?person wdt:P106 wd:Q121594  . " #Professor
MetropolitanQ       = "?person wdt:P106 wd:Q205375  . " #Inventor
DistrictQ           = "?person wdt:P106 wd:Q81096   . " #Engineer
PiccadillyQ         = "?person wdt:P106 wd:Q4964182 . " #Philosopher
VictoriaQ           = "?person wdt:P106 wd:Q11063   . " #Astronomer
JubileeQ            = "?person wdt:P106 wd:Q170790  . " #Mathematician
CentralQ            = "?person wdt:P20  wd:Q84      . " #Died in London
NorthernQ           = "{?person wdt:P101 wd:Q21198} UNION {?person wdt:P101 wd:Q11661} UNION {?person wdt:P106 wd:Q82594} " #CS
NotMaleQ            = "FILTER ( !EXISTS{ ?person wdt:P21 wd:Q6581097 }) " #Not Male

QueryStart = "SELECT DISTINCT ?person ?personLabel ?sitelinks WHERE {"
QueryEnd   = """
                  ?person wdt:P31 wd:Q5 .
                  ?person wikibase:sitelinks ?sitelinks .
                  SERVICE wikibase:label {
                      bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en" .
                  }
              } ORDER BY DESC (?sitelinks)
"""
stationCount = 0

for station in stationsAndLines:
	time.sleep(2)	#	Stop going over the rate limi. See https://github.com/RDFLib/sparqlwrapper/issues/138
	stationCount += 1
	stationName = list(station.keys())[0]
	lines  = list(list(station.values())[0].keys())
	print(str(stationCount) + " " + stationName)

	# 	Build the query
	QueryWhere = " "
	for line in lines:
		if line == "bakerloo":
			QueryWhere += BakerlooQ
		if line == "circle":
			QueryWhere += CircleQ
		if line == "hammersmith-city":
			QueryWhere += HammersmithAndCityQ
		if line == "waterloo-city":
			QueryWhere += WaterlooAndCityQ
		if line == "metropolitan":
			QueryWhere += MetropolitanQ
		if line == "district":
			QueryWhere += DistrictQ
		if line == "piccadilly":
			QueryWhere += PiccadillyQ
		if line == "victoria":
			QueryWhere += VictoriaQ
		if line == "jubilee":
			QueryWhere += JubileeQ
		if line == "central":
			QueryWhere += CentralQ
		if line == "northern":
			QueryWhere += NorthernQ

	# 	Get the data
	# 	First, find not males
	results = getData(QueryStart + QueryWhere + NotMaleQ + QueryEnd)

	if (0 == len(results["results"]["bindings"])):
		# 	If there are none, get some blokes
		print("No non-male data found")
		results = getData(QueryStart + QueryWhere + QueryEnd)

	positionFilled = False
	for result in results["results"]["bindings"]:
		URL  = result["person"]["value"]
		name = result["personLabel"]["value"]
		pair = {URL : name}
		if (pair in StationPersonMap.values()) :
			# on to the next one
			print(name + " is already present")
		else :
			StationPersonMap[stationName] = pair
			print("Added " + name + " to " + stationName)
			positionFilled = True
			break

	#	What to do if all the women have been used up?
	#	This should probably be optimised!
	if (False == positionFilled):
		print("All non-male data used up, trying some menfolk")
		results = getData(QueryStart + QueryWhere + QueryEnd)
		for result in results["results"]["bindings"]:
			URL  = result["person"]["value"]
			name = result["personLabel"]["value"]
			pair = {URL : name}
			if (pair in StationPersonMap.values()) :
				# on to the next one
				print(name + " is already present")
			else :
				StationPersonMap[stationName] = pair
				print("Added " + name + " to " + stationName)
				positionFilled = True
				break
		if (False == positionFilled):
			print("No candidate found for " + stationName)
			StationPersonMap[stationName] = "null"

	#	Quicksave!
	json.dump( StationPersonMap, open( "StationPersonMap.json", 'w' ) )

json.dump( StationPersonMap, open( "StationPersonMap.json", 'w' ) )
print(StationPersonMap)

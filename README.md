# Greater Bear - graph data of TfL tube stations
An attempt to improve [The Great Bear](https://www.tate.org.uk/art/artworks/patterson-the-great-bear-p77880) by Simon Patterson.

This uses data about London Underground stations.

> "Powered by TfL Open Data" and provided under [OGLv2](https://tfl.gov.uk/corporate/terms-and-conditions/transport-data-service). It may contain OS data © Crown copyright and database rights 2016.

Wikidata structured data from the main, Property, Lexeme, and EntitySchema namespaces is available under the <a href="//creativecommons.org/publicdomain/zero/1.0/" title="Definition of the Creative Commons CC0 License">Creative Commons CC0 License</a>; text in the other namespaces is available under the <a href="//creativecommons.org/licenses/by-sa/3.0/" title="Definition of the Creative Commons Attribution/Share-Alike License">Creative Commons Attribution-ShareAlike License</a>.

Hammersmith One font © 2011 by [Sorkin Type Co](http://www.sorkintype.com), with Reserved Font Name "Hammersmith", "Hammersmith One". [Licensed under the SIL Open Font License, Version 1.1](https://www.fontsquirrel.com/license/hammersmith-one).

Python <a href="https://gitlab.com/edent/greater-bear">source code by Terence Eden</a> made available under the <a href="https://opensource.org/licenses/MIT">MIT License</a>.

## Use

Run `python3 sparql.py`

That will:

* Get every line on the London Tube
* Get every station on those lines
* Create a dict of stations with the lines they are on, ordered by how many lines they are on
* Generate a Wikidata SPARQL query for every station
* Save a file mapping stations to people

## Errata

* The data in Wikidata may be incorrect or incomplete.
* I originally didn't restrict it to just humans! So a few weird entries snuck in. Using `?person wdt:P31 wd:Q5 .` corrected that. But I'm wondering if anyone on the map is *fictional*...!
* Due to timeouts and my crappy coding, I ran the code over several passes on different days. If you run the code, you might get different results.
* I didn't use people's names in their original language, I had to back-fill them. I probably missed some. I should have used [`P1559`](https://www.wikidata.org/wiki/Property:P1559).
* Even after lots of jiggling of categories, one or two stations kept coming up blank. So I manually added in a few people. Can you spot who they are?
* Some people's names were too long for the allotted space, so I have swapped a few people around. Better code would try to keep name length as close to the original as possible.
* There's no (intentional) ordering. It might be nice to put people on the line in order of, say, year of birth.
* Similarly, there's almost no relation between the people and the places. Although I've contrived to put the author of Mary Poppins somewhere special!
* The Hammersmith One font only has a basic set of characters - so non-European languages (and some accents) are in the default font.
* I've also added a couple of Easter Eggs. Enjoy finding them!

import requests
import csv

# Get all the current Tube lines
r = requests.get("https://api.tfl.gov.uk/Line/Mode/tube")
lines = r.json()


with open('Tube Lines.csv', 'wt') as tubecsv:
	writer = csv.writer(tubecsv)
	writer.writerow(["id", "name"])

	for line in lines:
		id = line["id"]
		name = line["name"]
		writer.writerow([id, name])
